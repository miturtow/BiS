## Interface: 90001
## Title: |cff42e6f5BiS|r
## Notes: Helps track your best-in-slot items.
## Author: miturtow
## Version: 1.2.0
## SavedVariables: BiSDB

embeds.xml
BiS.lua
