
local BiS = LibStub("AceAddon-3.0"):NewAddon("BiS", "AceEvent-3.0")
local AceGUI = LibStub("AceGUI-3.0")

local _G = _G
local EJ = _G["EncounterJournal"]

local pName
local mainMenu

function BiS:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New("BiSDB")
    self.db.char.playerName = pName
    self.db.char.
    print("BiS loaded.")
    mainMenu = AceGUI:Create("Frame")
    mainMenu:Hide()
end

function BiS:OnEnable()
    
end

function BiS:OnDisable()
    
end

--[[
mainFrame:RegisterEvent("ENCOUNTER_LOOT_RECEIVED")
mainFrame:RegisterEvent("BONUS_ROLL_RESULT")
mainFrame:RegisterEvent("QUEST_LOOT_RECEIVED")
mainFrame:RegisterEvent("MAIL_SUCCESS")
]]

local function AddItem(self, button, down)
    self.frame:SetNormalTexture("Interface\\COMMON\\FavoritesIcon")
    -- self:SetImageSize(24, 24)
    local item = self.frame:GetParent()
    local id = item.itemID
    local link = item.link
    if not BLtable[id] then
        BLtable[id] = link
        print("Added", link, id, "to the BiS list.")
    else
        print(link, "is already present in your BiS list.")
    end
end

function OpenMenu(args)
    mainMenu:Show()
end


--[[
local function FrameAtCursor()
    local f = CreateFrame("Frame", nil, UIParent, BackdropTemplateMixin and "BackdropTemplate")
    f:SetFrameStrata("HIGH")
    -- f:SetFrameLevel(5)
    -- f:SetTopLevel(true)
    local scale, x, y = f:GetEffectiveScale(), GetCursorPosition()
    f:SetPoint("LEFT", nil, "BOTTOMLEFT", x / scale, y / scale)
    f:SetSize(128, 128)
    f:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        edgeSize = 16,
        insets = {
            left = 4,
            right = 4,
            top = 4,
            bottom = 4
        }
    })
    f:SetBackdropColor(0, 0, 0, .5)
end
]]
function BiS:ADDON_LOADED(event, ...)
    if ... == "Blizzard_EncounterJournal" then
        for i = 1, 10 do
            local item = _G["EncounterJournalEncounterFrameInfoLootScrollFrameButton" .. i]
            local button = AceGUI:Create("Button")
            local id = item.itemID
            item.Button = { button, id }
            button.frame:SetNormalTexture("Interface\\COMMON\\FavoritesIcon")
            button.frame:SetSize(24, 24)
            button:SetCallback("OnClick", OpenMenu)
            button.frame:SetParent(item)
            button.frame:SetPoint("TOPRIGHT")
            button.frame:Show()
        end
    end
end

function BiS:CHAT_MSG_LOOT(event, ...)
    local text, playerName, languageName, channelName, playerName2, specialFlags, zoneChannelID, channelIndex,
        channelBaseName, unused, lineID, guid, bnSenderID, isMobile, isSubtitle, hideSenderInLetterbox,
        supressRaidIcons = ...
    -- print("CHAT_MSG_LOOT", text, playerName)
    local printable = gsub(text, "\|", "\|\|")
    local itemName, itemLink, itemRarity, itemLevel, itemMinLevel, itemType, itemSubType, itemStackCount,
    itemEquipLoc, itemIcon, itemSellPrice, itemClassID, itemSubClassID, bindType, expacID, itemSetID, 
    isCraftingReagent = GetItemInfo(text)
    print(itemName, itemLink)
    print(printable)
    local i, j = string.find(text, "%[.*%]")
    local itemName = string.sub(text, i + 1, j - 1)
    -- local o, k = string.find(text, "Hitem:(%d+):")
    -- print("o:", o, "k:", k)
    -- local itemID = string.sub(text, o + 6, k - 1)
    local p, l = string.find(playerName, "%a+%-")
    local shortName = string.sub(playerName, p, l - 1)
    if pName == shortName then
        print(shortName, itemName, itemID)
    end
    
    if BLtable[tonumber(itemID)] then
        print("Congrats with your BiS:", itemName)
    end
    
end

BiS:RegisterEvent("ADDON_LOADED")
BiS:RegisterEvent("CHAT_MSG_LOOT")